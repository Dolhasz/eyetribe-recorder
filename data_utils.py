import json
import os
import matplotlib.pyplot as plt
import datetime
import numpy as np


class SubjectEyeData:
    def __init__(self, path):
        self.path = path
        with open(self.path, 'r') as f:
            self.data = json.load(f)

        self.get_xy_frames()

    def get_timestamps(self):
        return [
            (frame['values']['frame']['timestamp'], frame['values']['frame']['avg']['x'], frame['values']['frame']['avg']['y'], frame['values']['frame']['fix']) 
            for frame in self.data]

    def get_xy_frames(self):
        self.xs = []
        self.ys = []
        
        for frame in self.data:
            self.xs.append(frame['values']['frame']['avg']['x'])
            self.ys.append(frame['values']['frame']['avg']['y'])

    def visualise(self):
        plt.scatter(self.xs, self.ys)
        plt.xlim(0,1920)
        plt.ylim(0,1080)
        # plt.gca().invert_xaxis()
        plt.gca().invert_yaxis()
        plt.show()



if __name__ == '__main__':

    # Fix this --> save gaze data with different extension

    # for subject_folder in os.listdir('./results'):
    #     path = os.path.join('./results', subject_folder)

    #     if os.path.isdir(path):
    #         for im_gaze_data in os.listdir(path):
    #             if im_gaze_data.endswith('.gaze') :
    #                 subject = SubjectEyeData(os.path.join(path, im_gaze_data))
    #                 subject.get_xy_frames()
    #                 subject.xs = np.clip(subject.xs, 0, 1920)
    #                 subject.ys = np.clip(subject.ys, 0, 1080)
    #                 subject.visualise()

    subject = SubjectEyeData(os.path.join('./results', 'XX77XX', 'first_image.gaze'))
    ts = subject.get_timestamps()



    t, x, y = [datetime.datetime.strptime(d[0], '%Y-%m-%d %H:%M:%S.%f') for d in ts], [d[1] for d in ts], [d[2] for d in ts]



