import socket
import json
import os
import tkinter as tk 
import numpy as np
from tkinter import font as tkfont
import time
import threading


class EyeTrackerThread(threading.Thread):
    
    def __init__(self, path, image_id, *args, **kwargs):
        super(EyeTrackerThread, self).__init__(*args, **kwargs)
        self._stop = threading.Event()
        self.frame_data = bytearray()
        self.path = path
        self.image_id = image_id

    def stop(self):
        self._stop.set()
        print('Stopped Recording')

    def stopped(self):
        return self._stop.isSet()

    def run(self):
        print('Running thread')

        # Setup Host & Request
        host_ip, server_port = "127.0.0.1", 6555
        req = {
            "category": "tracker",
            "request": "get",
            "values": ["frame"]
        }
        b = json.dumps(req).encode()

        # Create and connect to socket
        tcp_client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        tcp_client.connect((host_ip, server_port))

        # Request eye data while active
        while not self.stopped():
            try:
                # Send request
                tcp_client.send(b)

                # Read data from socket
                received = tcp_client.recv(4096)
                self.frame_data.extend(received)

            finally:
                pass

            time.sleep(.1) # Sleep briefly so we don't hog CPU cycles

        self.save_data()

    def save_data(self):

        j = self.frame_data.decode()

        found = j.split(u'\n')

        final_output = []

        for i in found:

            if len(i) > 0:
                try:
                    js = json.loads(i)
                    final_output.append(js)
                except Exception as error:
                    with open('{}/{}.gaze_BACKUP'.format(self.path, self.image_id), 'w') as f:
                        f.writelines(j)


        if not os.path.exists(self.path):
            os.makedirs(self.path)
        
        with open('{}/{}.gaze'.format(self.path, self.image_id), 'w') as f:
            json.dump(final_output, f, indent=4)
                

class EyeDataRecorderApp(tk.Tk):
    '''
    Main application, holds container and other top-level stuff
    '''

    def __init__(self, *args, **kwargs):
        tk.Tk.__init__(self, *args, **kwargs)
        self.title_font = tkfont.Font(
            family=np.random.choice(tkfont.families()), 
            size=24,
            weight="bold", 
            slant="roman"
            )

        self.label = tk.Label(self, text='Enter filename')
        self.label.pack()
        self.path = tk.Entry(self)
        self.path.pack()

        self.b1 = tk.Button(
            master=self, 
            text="Start Recording",
            command=lambda: self.recording_start()
            )
        self.b1.pack()

        self.b2 = tk.Button(
            master=self, 
            text="Stop Recording",
            command=lambda: self.recording_stop()
            )
        self.b2.pack()

class GazeRecorder:
    def __init__(self, recording_path, image_id='_'):
        self.path = recording_path
        self.image_id = image_id

    def start(self):
        self.recorder = EyeTrackerThread(path=self.path, image_id=self.image_id)
        print('Starting Recording')
        self.recorder.start()

    def stop(self):
        self.recorder.stop()


if __name__ == '__main__':
    m = EyeDataRecorderApp()
    m.mainloop()